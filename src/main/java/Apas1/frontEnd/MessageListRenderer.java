package Apas1.frontEnd;

import model.Message;

import javax.swing.*;
import java.awt.*;

public class MessageListRenderer implements ListCellRenderer {

    private JPanel panel;
    private JLabel label;

    private Color selectedColor;
    private Color normalColor;

    public MessageListRenderer() {
        panel = new JPanel();
        label = new JLabel();
        selectedColor = new Color(210, 210, 255);
        normalColor = Color.white;

        label.setIcon(new ImageIcon("src\\main\\resources\\images\\Info.png"));

        panel.setLayout(new FlowLayout(FlowLayout.LEFT));

        panel.add(label);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {

        Message message = (Message) value;

        label.setText(message.getTitle());

        panel.setBackground(hasFocus ? selectedColor : normalColor);
        label.setOpaque(true);

        return panel;
    }
}
