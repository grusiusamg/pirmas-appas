package Apas1.frontEnd;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

public class ServerTreeCellRenderer implements TreeCellRenderer {
    private JCheckBox leafRenderer;
    private DefaultTreeCellRenderer nonLeafRenderer;
    private Color textForeGround;
    private Color textBackGround;
    private Color selectionForeGround;
    private Color selectionBackground;

    public ServerTreeCellRenderer() {
        leafRenderer = new JCheckBox();
        nonLeafRenderer = new DefaultTreeCellRenderer();

        nonLeafRenderer.setLeafIcon(new ImageIcon("src\\main\\resources\\images\\Server.png"));
        nonLeafRenderer.setOpenIcon(new ImageIcon("src\\main\\resources\\images\\Web.png"));
        nonLeafRenderer.setClosedIcon(new ImageIcon("src\\main\\resources\\images\\WebSelc.png"));

        textForeGround = UIManager.getColor("Tree.textForeground");
        textBackGround = UIManager.getColor("Tree.textBackGround");
        selectionForeGround = UIManager.getColor("Tree.selectionForeGround");
        selectionBackground = UIManager.getColor("Tree.selectionBackground");
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if (leaf) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            ServerInfo nodeInfo = (ServerInfo) node.getUserObject();

            if(selected){
                leafRenderer.setForeground(selectionForeGround);
                leafRenderer.setBackground(selectionBackground);
            }else{
                leafRenderer.setForeground(textForeGround);
                leafRenderer.setBackground(textBackGround);
            }

            leafRenderer.setText(nodeInfo.toString());
            leafRenderer.setSelected(nodeInfo.isChecked());
            return leafRenderer;
        } else {
            return nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
    }
}
