package Apas1.frontEnd;

import java.util.EventListener;

public interface FormListener extends EventListener {
    public void formEventOccurred(FormEvent e);
}
