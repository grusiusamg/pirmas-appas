package Apas1.frontEnd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class ProgressDialog extends JDialog {

    private JButton cancelButton;
    private JProgressBar progressBar;
    private ProgressDialogListener listener;

    public ProgressDialog(Window parent, String title) {

        super(parent, title, ModalityType.APPLICATION_MODAL);

        cancelButton = new JButton("Baigti");
        progressBar = new JProgressBar();
        progressBar.setStringPainted(true); //dadeda teksta

        progressBar.setMaximum(10);

        progressBar.setString("Gaunami pranesimai");

        //progressBar.setIndeterminate(true);

        setLayout(new FlowLayout());

        Dimension size = cancelButton.getPreferredSize();
        size.width = 400;
        progressBar.setPreferredSize(size);

        add(progressBar);
        add(cancelButton);

        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (listener != null) {
                    listener.progressDialogCanceled();
                }
            }
        });
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent windowEvent) {

            }

            @Override
            public void windowClosing(WindowEvent windowEvent) {
                if (listener != null) {
                    listener.progressDialogCanceled();
                }
            }

            @Override
            public void windowClosed(WindowEvent windowEvent) {

            }

            @Override
            public void windowIconified(WindowEvent windowEvent) {

            }

            @Override
            public void windowDeiconified(WindowEvent windowEvent) {

            }

            @Override
            public void windowActivated(WindowEvent windowEvent) {

            }

            @Override
            public void windowDeactivated(WindowEvent windowEvent) {

            }
        });

        pack();

        setLocationRelativeTo(parent);

    }

    public void setListener(ProgressDialogListener listener) {
        this.listener = listener;
    }

    public void setMaximum(int value) {
        progressBar.setMaximum(value);
    }

    public void setValue(int value) {
        progressBar.setValue(value);

        int progress = 100 * value / progressBar.getMaximum();

        progressBar.setString(String.format("%d%% complete", progress));//dadeda procentus

        progressBar.setValue(value);
    }

    @Override
    public void setVisible(final boolean visible) {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // System.out.println("Showing modal dialog"); //testas

                // System.out.println("Finish showing modal dialog");//testas

                if (visible == false) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setValue(0);
                }

                if (visible) {
                    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                } else {
                    setCursor(Cursor.getDefaultCursor());
                }

                ProgressDialog.super.setVisible(visible);

            }
        });
    }
}
