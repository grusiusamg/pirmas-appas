package Apas1.frontEnd;

import controller.Controller;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.prefs.Preferences;

public class MainFrame extends JFrame {

    // private TextPanel textPanel;
    private Toolbar toolbar;
    private FormPanel formPanel;
    private JFileChooser fileChooser;
    private Controller controller;
    private TablePanel tablePanel;
    private PrefsDialog prefsDialog;
    private Preferences prefs;
    private JSplitPane splitPane;
    private JTabbedPane tabbedPane;
    private MessagePanel messagePanel;


    public JRadioButton ltLanguage;
    public JRadioButton engLanguage;
    public ButtonGroup languageButtonGroup;

    public MainFrame() {
        super("Pirmas Appas");

        setLayout(new BorderLayout());

        toolbar = new Toolbar();
        //textPanel = new TextPanel();
        formPanel = new FormPanel();
        tablePanel = new TablePanel();
        prefsDialog = new PrefsDialog(this);
        tabbedPane = new JTabbedPane();
        messagePanel = new MessagePanel(this);
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, formPanel, tabbedPane);

        splitPane.setOneTouchExpandable(true);

        tabbedPane.addTab("Asmens DB", tablePanel);
        tabbedPane.addTab("Pranesimai", messagePanel);


        prefs = Preferences.userRoot().node("db");

        controller = new Controller();
        tablePanel.setData(controller.getPeople());

        tablePanel.setPersonTableListener(new PersonTableListener() {
            public void rowDeleted(int row) {
                controller.removePerson(row);
            }
        });

        tabbedPane.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
                int tabIndex = tabbedPane.getSelectedIndex();

                if (tabIndex == 1) {
                    messagePanel.refresh();
                }
            }
        });

        prefsDialog.setPrefsListener(new PrefsListener() {
            public void preferenceSet(String user, String password, int port) {
                prefs.put("user", user);
                prefs.put("password", password);
                prefs.putInt("port", port);

                try {
                    controller.configure(port,user,password);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(MainFrame.this,"Nepavyko prisijungti prie duombazes");
                }

            }
        });

        String user = prefs.get("user", "");
        String password = prefs.get("password", "");
        Integer port = prefs.getInt("port: ", 3306);

        prefsDialog.setDefaults(user, password, port);

        fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new PersonFileFilter());

        setJMenuBar(createMenuBar());

        toolbar.setToolbarListener(new ToolbarListener() {
            public void saveEventOccured() {
                connect();
                try {
                    controller.save();
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(MainFrame.this, "nepavyksta irasyti i duombaze", "Duombazes irasimo problema", JOptionPane.ERROR_MESSAGE);

                }
                // System.out.println("SAVE");//testas
            }

            public void refreshEventOccured() {
                connect();
                try {
                    controller.load();
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(MainFrame.this, "nepavyksta uzkrauti is duombazes", "Duombazes uzkrovimo problema", JOptionPane.ERROR_MESSAGE);

                }
                tablePanel.refresh();
                // System.out.println("REFRESH");//testas
            }
        });

        formPanel.setFormListener(new FormListener() {
            public void formEventOccurred(FormEvent e) {
//
                controller.addPerson(e);
                tablePanel.refresh();
            }
        });

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //  System.out.println("uzdaromas langas");//testas
                controller.disconnect();
                dispose();
                System.gc();

            }
        });


        add(toolbar, BorderLayout.PAGE_START);
        add(splitPane, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setMinimumSize(new Dimension(400, 400));
        setSize(1050, 400);

        setVisible(true);
    }

    private void connect() {
        try {
            controller.connect();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(MainFrame.this, "nepavyksta prisijungti prie duombazes", "Duombazes prisijungimo problema", JOptionPane.ERROR_MESSAGE);
        }
    }

    //meniu baras
    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();


        JMenu fileMenu = new JMenu("File");
        JMenuItem exportDataItem = new JMenuItem("Export Data...");
        JMenuItem importDataItem = new JMenuItem("Imporft Data...");
        JMenuItem exitItem = new JMenuItem("Exit");

        fileMenu.add(exportDataItem);
        fileMenu.add(importDataItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);

        JMenu windowMenu = new JMenu("Window");
        JMenu showMenu = new JMenu("Show");
        JMenuItem prefsItem = new JMenuItem("Preferences...");
        JCheckBoxMenuItem showFormItem = new JCheckBoxMenuItem("Person form");
        showFormItem.setSelected(true);

        showMenu.add(showFormItem);
        windowMenu.add(showMenu);
        windowMenu.add(prefsItem);


        JMenu languageMenu = new JMenu("Language");


        ltLanguage = new JRadioButton("Lietuviu");
        engLanguage = new JRadioButton("English");


        languageButtonGroup = new ButtonGroup();
        languageButtonGroup.add(ltLanguage);
        languageButtonGroup.add(engLanguage);
        ltLanguage.setActionCommand("lt");
        engLanguage.setActionCommand("en");
        engLanguage.setSelected(true);


        languageMenu.add(ltLanguage);
        languageMenu.add(engLanguage);

        menuBar.add(fileMenu);
        menuBar.add(windowMenu);
        menuBar.add(languageMenu);

        prefsItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                prefsDialog.setVisible(true);
            }
        });

        showFormItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem) ev.getSource();

                if (menuItem.isSelected()) {
                    splitPane.setDividerLocation((int) formPanel.getMinimumSize().getWidth());
                }
                formPanel.setVisible(menuItem.isSelected());
            }
        });

        fileMenu.setMnemonic(KeyEvent.VK_F); // ijungia ALT +F iskvietima
        exitItem.setMnemonic(KeyEvent.VK_X);
        exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK)); //ijungia CTR + X valdyma
        importDataItem.setMnemonic(KeyEvent.VK_I);
        importDataItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
        exportDataItem.setMnemonic(KeyEvent.VK_E);
        exportDataItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        prefsItem.setMnemonic(KeyEvent.VK_P);
        prefsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));

        importDataItem.addActionListener(e -> {
            if (fileChooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION) {
                try {
                    controller.loadFromFile(fileChooser.getSelectedFile());
                    tablePanel.refresh();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(MainFrame.this, "Could not load data from file.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        exportDataItem.addActionListener(e -> {
            if (fileChooser.showSaveDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION) {
                try {
                    controller.saveToFile(fileChooser.getSelectedFile());
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(MainFrame.this,
                            "Could not save to  file.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }


            }
        });


        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {

//                JOptionPane.showInputDialog(MainFrame.this,
//                        "Iveskite savo vartotojo varda", "Vartotojo vardas",
//                        JOptionPane.OK_OPTION|JOptionPane.QUESTION_MESSAGE); //pakeicia ikona
//

                int action = JOptionPane.showConfirmDialog(MainFrame.this,
                        "Ar tikrai norite uzdaryti?", "patvirtinti uzdaryma", JOptionPane.OK_CANCEL_OPTION);

                if (action == JOptionPane.OK_OPTION) {
                    WindowListener[] listeners = getWindowListeners();
                    for (WindowListener listener : listeners) {
                        listener.windowClosing(new WindowEvent(MainFrame.this, 0));
                    }
                }
            }
        });

        return menuBar;

    }

}
