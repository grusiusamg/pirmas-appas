package Apas1.frontEnd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Toolbar extends JToolBar implements ActionListener {
    private JButton saveButton;
    private JButton refreshButton;

    private ToolbarListener textListener;

    public Toolbar() {
        //setBorder(BorderFactory.createEtchedBorder());
        saveButton = new JButton();
        saveButton.setIcon(new ImageIcon("src\\main\\resources\\images\\Save.png"));
        saveButton.setToolTipText("Issaugoti");

        refreshButton = new JButton();
        refreshButton.setIcon(new ImageIcon("src\\main\\resources\\images\\Refresh.png"));
        refreshButton.setToolTipText("Atnaujinti");

        saveButton.addActionListener(this);
        refreshButton.addActionListener(this);
        setLayout(new FlowLayout(FlowLayout.LEFT));

        add(saveButton);
        //addSeparator();
        add(refreshButton);
    }
///////////////////////////////////kazkas neveikia
//    private ImageIcon createIcon(String path) {
//        URL url = getClass().getResource(path);
//
//        if(url==null){
//            System.out.println("nepavyko uzkrauti paveikslelio: " + path);
//        }
//
//        ImageIcon icon = new ImageIcon(url);
//        return icon;
//    }

    public void setToolbarListener(ToolbarListener listener) {
        this.textListener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clicked = (JButton) e.getSource();

        if (clicked == saveButton) {
            if (textListener != null) {
                textListener.saveEventOccured();
            }

        } else if (clicked == refreshButton) {
            if (textListener != null) {
                textListener.refreshEventOccured();
            }
        }
    }
}
