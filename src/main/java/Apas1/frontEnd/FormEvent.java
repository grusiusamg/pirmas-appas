package Apas1.frontEnd;

import java.util.EventObject;

public class FormEvent extends EventObject {

    private String name;
    private String occupation;
    private int ageCategory;
    private String empCat;
    private String taxId;
    private boolean ltCitizen;
    private String gender;


    public FormEvent(Object source) {
        super(source);
    }

    public FormEvent(Object source, String name, String occupation, int ageCategory, String empCat,
                     String taxId, boolean ltCitizen, String gender) {
        super(source);
        this.name = name;
        this.occupation = occupation;
        this.ageCategory = ageCategory;
        this.empCat = empCat;
        this.taxId = taxId;
        this.ltCitizen = ltCitizen;
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getTaxId() {
        return taxId;
    }

    public boolean isLtCitizen() {
        return ltCitizen;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public int setAgeCategory() {
        return ageCategory;
    }

    public String getEmpCat() {
        return empCat;
    }
}
