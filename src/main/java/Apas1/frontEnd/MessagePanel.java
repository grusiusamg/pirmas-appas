package Apas1.frontEnd;

import controller.MessageServer;
import model.Message;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static javafx.scene.input.KeyCode.T;

class ServerInfo {
    private String name;
    private int id;
    private boolean checked;

    public ServerInfo(String name, int id, boolean checked) {
        this.name = name;
        this.id = id;
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}

public class MessagePanel extends JPanel implements ProgressDialogListener {
    private JTree serverTree;
    private ServerTreeCellRenderer treeCellRenderer;
    private ServerTreeCellEditor treeCellEditor;

    private Set<Integer> selectServers;
    private MessageServer messageServer;
    private ProgressDialog progressDialog;
    private SwingWorker<List<Message>, Integer> worker;

    private TextPanel textPanel;
    private JList messageList;
    private JSplitPane upperPane;
    private JSplitPane lowerPane;
    private DefaultListModel messageListModel;


    public MessagePanel(JFrame parent) {

        messageListModel = new DefaultListModel();
        progressDialog = new ProgressDialog(parent, "Gaunami pranesimai");
        messageServer = new MessageServer();

        progressDialog.setListener(this);

        selectServers = new TreeSet<Integer>();
        selectServers.add(0);
        selectServers.add(1);
        selectServers.add(4);

        treeCellRenderer = new ServerTreeCellRenderer();
        treeCellEditor = new ServerTreeCellEditor();


        serverTree = new JTree(createTree());
        serverTree.setCellRenderer(treeCellRenderer);
        serverTree.setCellEditor(treeCellEditor);
        serverTree.setEditable(true);

        serverTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);//leidzia tik viena pasirinkima

        messageServer.setSelectedServers(selectServers);

        treeCellEditor.addCellEditorListener(new CellEditorListener() {
            public void editingStopped(ChangeEvent changeEvent) {
                ServerInfo info = (ServerInfo) treeCellEditor.getCellEditorValue();
                // System.out.println(info + ": " + info.getId() + "; " + info.isChecked());//testas

                int serverId = info.getId();

                if (info.isChecked()) {
                    selectServers.add(serverId);
                } else {
                    selectServers.remove(serverId);
                }

                messageServer.setSelectedServers(selectServers);

                retrieveMessage();

            }

            public void editingCanceled(ChangeEvent changeEvent) {
            }
        });

        setLayout(new BorderLayout());
// serveriu textu langai
        textPanel = new TextPanel();
        messageList = new JList(messageListModel);
        messageList.setCellRenderer(new MessageListRenderer());

        messageList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                Message message = (Message) messageList.getSelectedValue();

                textPanel.setText(message.getContents());
            }
        });

        lowerPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(messageList), textPanel);
        upperPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(serverTree), lowerPane);

        textPanel.setMinimumSize(new Dimension(10, 40));
        messageList.setMinimumSize(new Dimension(10, 40));

        upperPane.setResizeWeight(0.5);
        lowerPane.setResizeWeight(0.5);

        add(upperPane, BorderLayout.CENTER);

    }

    public void refresh() {
        retrieveMessage();
    }

    private void retrieveMessage() {

        progressDialog.setMaximum(messageServer.getMessageCount());
        //System.out.println("Message is waiting " + messageServer.getMessageCount()); //testas

        progressDialog.setVisible(true);

        worker = new SwingWorker<List<Message>, Integer>() {
            @Override
            protected List<Message> doInBackground() throws Exception {

                List<Message> retrieveMessages = new ArrayList<Message>();

                int count = 0;
                for (Message message : messageServer) {

                    if (isCancelled()) break;
                    System.out.println(message.getTitle());

                    retrieveMessages.add(message);
                    count++;
                    publish(count);
                }
                return retrieveMessages;
            }

            @Override
            protected void process(List<Integer> counts) {
                int retrieved = counts.get(counts.size() - 1);

                progressDialog.setValue(retrieved);
                // System.out.println("Got " + retrieved + " messages.");//testas
            }

            @Override
            protected void done() {
                progressDialog.setVisible(false);

                if (isCancelled()) return;
                try {
                    List<Message> retrieveMessages = get();

                    messageListModel.removeAllElements();

                    for (Message message : retrieveMessages) {
                        messageListModel.addElement(message);
                    }

                    messageList.setSelectedIndex(0);//parenka pati pirma pasirinkima
                    // System.out.println("Retrieved " + retrieveMessages.size() + " messages");//testas
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

        };

        worker.execute();


    }

    private DefaultMutableTreeNode createTree() {
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("Servers");

        DefaultMutableTreeNode branch1 = new DefaultMutableTreeNode("JAV");
        DefaultMutableTreeNode server1 = new DefaultMutableTreeNode(new ServerInfo("New York", 0, selectServers.contains(0)));
        DefaultMutableTreeNode server2 = new DefaultMutableTreeNode(new ServerInfo("Boston", 1, selectServers.contains(1)));
        DefaultMutableTreeNode server3 = new DefaultMutableTreeNode(new ServerInfo("Los Angeles", 2, selectServers.contains(2)));
        branch1.add(server1);
        branch1.add(server2);
        branch1.add(server3);

        DefaultMutableTreeNode branch2 = new DefaultMutableTreeNode("UK");
        DefaultMutableTreeNode server4 = new DefaultMutableTreeNode(new ServerInfo("London", 3, selectServers.contains(3)));
        DefaultMutableTreeNode server5 = new DefaultMutableTreeNode(new ServerInfo("Edinburgh", 4, selectServers.contains(4)));
        branch2.add(server4);
        branch2.add(server5);

        DefaultMutableTreeNode branch3 = new DefaultMutableTreeNode("EU");
        DefaultMutableTreeNode server6 = new DefaultMutableTreeNode(new ServerInfo("Vilnius", 5, selectServers.contains(5)));
        DefaultMutableTreeNode server7 = new DefaultMutableTreeNode(new ServerInfo("Ryga", 6, selectServers.contains(6)));
        branch3.add(server6);
        branch3.add(server7);

        top.add(branch1);
        top.add(branch2);
        top.add(branch3);

        return top;
    }

    @Override
    public void progressDialogCanceled() {

        if (worker != null) {
            worker.cancel(true);
        }
        // System.out.println("canceled");//testas
    }
}

