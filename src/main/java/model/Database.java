package model;

import java.io.*;
import java.sql.*;
import java.util.*;


public class Database {

    ResourceBundle res = ResourceBundle.getBundle("PrefsDialog");

    private List<Person> people;
    private Connection con;

    private int port;
    private String user;
    private String password;

   // String connectionUrl = "jdbc:mysql://localhost:" + res.getString("connection") + "/pirmasAppas";

    // String user = res.getString("name");
    // String password = res.getString("password");

    public Database() {
        people = new LinkedList<Person>();
    }

    public void configure(int port, String user, String password) {

        this.port = port;
        this.user = user;
        this.password = password;

    }

    public void connect() throws Exception {
        String connectionUrl = "jdbc:mysql://localhost:" + port + "/pirmasAppas";
        if (con != null) return;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new Exception("Driveris nerastas");
        }

        con = DriverManager.getConnection(connectionUrl, user, password);
        System.out.println("Prisijungta: " + con);//mano testavimas
    }

    public void disconnect() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println("Neisejo atjungti rysio");
            }
        }

    }

    public void save() throws SQLException {

        String checkSql = "Select count(*) as count from people where id=?";
        PreparedStatement checkStmt = con.prepareStatement(checkSql);

        String insertSql = "insert into people (name, age, employmentStatus, taxId, LtCitizen, gender, occupation) values(?,?,?,?,?,?,?)";
        PreparedStatement insertStatement = con.prepareStatement(insertSql);

        String updateSql = "update people set name=?, age=?, employmentStatus=?, taxId=?, LtCitizen=?, gender=?, occupation=? where id=?";
        PreparedStatement updateStatement = con.prepareStatement(updateSql);

        for (Person person : people) {
            int id = person.getId();
            String name = person.getName();
            String occupation = person.getOccupation();
            AgeCategory age = person.getAgeCategory();
            EmploymentCategory emp = person.getEmpCat();
            String tax = person.getTaxId();
            boolean isLt = person.isLtCitizen();
            Gender gender = person.getGender();

            checkStmt.setInt(1, id);

            ResultSet checkResult = checkStmt.executeQuery();
            checkResult.next();

            int count = checkResult.getInt(1);

            if (count == 0) {
                System.out.println("Inserting person with ID " + id);

                int col = 1;
                insertStatement.setInt(col, id);
                insertStatement.setString(col++, name);
                insertStatement.setString(col++, age.name());
                insertStatement.setString(col++, emp.name());
                insertStatement.setString(col++, tax);
                insertStatement.setBoolean(col++, isLt);
                insertStatement.setString(col++, gender.name());
                insertStatement.setString(col++, occupation);

                insertStatement.executeUpdate();
            } else {
                System.out.println("Updating person with ID " + id);

                int col = 1;

                updateStatement.setString(col++, name);
                updateStatement.setString(col++, age.name());
                updateStatement.setString(col++, emp.name());
                updateStatement.setString(col++, tax);
                updateStatement.setBoolean(col++, isLt);
                updateStatement.setString(col++, gender.name());
                updateStatement.setString(col++, occupation);
                updateStatement.setInt(col, id);

                updateStatement.executeUpdate();
            }

        }
        updateStatement.close();
        insertStatement.close();
        checkStmt.close();
    }

    public void load() throws SQLException {
        people.clear();

        String sql = "select id, name, age, employmentStatus, taxId, LtCitizen, gender, occupation from people order by name";
        Statement selectStatement = con.createStatement();

        ResultSet results = selectStatement.executeQuery(sql);

        while (results.next()) {
            int id = results.getInt("id");
            String name = results.getString("name");
            String age = results.getString("age");
            String emp = results.getString("employmentStatus");
            String taxId = results.getString("taxId");
            boolean isLt = results.getBoolean("LtCitizen");
            String gender = results.getString("gender");
            String occ = results.getString("occupation");


            Person person = new Person(id, name, occ, AgeCategory.valueOf(age), EmploymentCategory.valueOf(emp), taxId, isLt, Gender.valueOf(gender));
            people.add(person);


            // System.out.println(person);//testas
        }


        results.close();
        selectStatement.close();
    }


    public void addPerson(Person person) {
        people.add(person);
    }

    public void removePerson(int index) {
        people.remove(index);
    }

    public List<Person> getPeople() {
        return Collections.unmodifiableList(people);
    }

    public void saveToFile(File file) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Person[] persons = people.toArray(new Person[people.size()]);

        oos.writeObject(persons);

        oos.close();
    }

    public void loadFromFile(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);

        try {
            Person[] persons = (Person[]) ois.readObject();

            people.clear();

            people.addAll(Arrays.asList(persons));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        ois.close();
    }

/////////////////////////////////////////////sutvarkyti
    public void removePerson2(int i) {
        String connectionUrl = "jdbc:mysql://localhost:" + port + "/pirmasAppas";

        String sql = "delete from people where id=" + i;

        try (Connection conn = DriverManager.getConnection(connectionUrl, user, password);
             Statement stmt = conn.createStatement();) {

            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


}
