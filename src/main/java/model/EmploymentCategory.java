package model;

public enum EmploymentCategory {
    employed("Dirbantis"),
    selfEmployed("Dirbantis sau"),
    unEmployed("Bedarbis"),
    other("Kitkas");

    private String text;

    private EmploymentCategory(String text){
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
