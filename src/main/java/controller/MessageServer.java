package controller;

import model.Message;

import java.util.*;

/*
This is a sort of simulated message server
 */
public class MessageServer implements Iterable<Message> {
    private Map<Integer, List<Message>> messages;

    private List<Message> selected;

    public MessageServer() {
        selected = new ArrayList<Message>();
        messages = new TreeMap<Integer, List<Message>>();

        List<Message> list = new ArrayList<>();
        list.add(new Message("The cat is missing", "Have you seen Felix anywhere?"));
        list.add(new Message("See you later?", "Are we still meeting in the pud?"));
        messages.put(0, list);

        list = new ArrayList<>();
        list.add(new Message("The dog is missing", "Have you seen Scoobydoo anywhere?"));
        list.add(new Message("See you later?", "Are we still meeting in the pud?"));
        messages.put(1, list);

        list = new ArrayList<>();
        list.add(new Message("The car is missing", "Have you seen lamborghini anywhere?"));
        list.add(new Message("See you later?", "Are we still meeting in the pud?"));
        messages.put(2, list);

        list = new ArrayList<>();
        list.add(new Message("The boat is missing", "Have you seen boat anywhere?"));
        list.add(new Message("See you later?", "Are we still meeting in the pud?"));
        messages.put(5, list);

        list = new ArrayList<>();
        list.add(new Message("The bottle is missing", "Have you seen beer bottle anywhere?"));
        list.add(new Message("See you later?", "Are we still meeting in the pud?"));
        messages.put(6, list);
    }

    public void setSelectedServers(Set<Integer> servers) {

        selected.clear();

        for (Integer id : servers) {
            if (messages.containsKey(id)) {
                selected.addAll(messages.get(id));
            }
        }
    }

    public int getMessageCount() {
        return selected.size();
    }

    @Override
    public Iterator<Message> iterator() {
        return new MessageIterator(selected);
    }
}

class MessageIterator implements Iterator {

    private Iterator<Message> iterator;

    public MessageIterator(List<Message> messages) {
        iterator = messages.iterator();

    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public Object next() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        return iterator.next();
    }

    @Override
    public void remove() {
        iterator.remove();
    }
}