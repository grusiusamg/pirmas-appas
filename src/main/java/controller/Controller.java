package controller;

import Apas1.frontEnd.FormEvent;
import model.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Controller {
    Database db = new Database();

    public List<Person> getPeople() {
        return db.getPeople();
    }

    public void removePerson(int index) {
        db.removePerson(index);
    }

    public void save() throws SQLException {
        db.save();
    }

    public void load() throws SQLException {
        db.load();
    }

    public void configure(int port, String user, String password) throws Exception {
        db.configure(port, user, password);
    }

    public void connect() throws Exception {
        db.connect();
    }

    public void disconnect() {
        db.disconnect();
    }

    public void addPerson(FormEvent ev) {
        String name = ev.getName();
        String occupation = ev.getOccupation();
        int ageCatId = ev.setAgeCategory();
        String empCat = ev.getEmpCat();
        boolean isLtCitizen = ev.isLtCitizen();
        String gender = ev.getGender();
        String taxId = ev.getTaxId();

        AgeCategory ageCategory = null;

        switch (ageCatId) {
            case 0:
                ageCategory = AgeCategory.child;
                break;
            case 1:
                ageCategory = AgeCategory.adult;
                break;
            case 2:
                ageCategory = AgeCategory.senior;
                break;

            default:

        }

        EmploymentCategory empCategory;

        if (empCat.equals("Dirbantis")) {
            empCategory = EmploymentCategory.employed;
        } else if (empCat.equals("Dirbantis sau")) {
            empCategory = EmploymentCategory.selfEmployed;
        } else if (empCat.equals("Bedarbis")) {
            empCategory = EmploymentCategory.unEmployed;
        } else {
            empCategory = EmploymentCategory.other;
            System.err.println(empCat);
        }

        Gender genderCat;

        if (gender.equals("Vyras")) {
            genderCat = Gender.male;
        } else {
            genderCat = Gender.female;
        }

        Person person = new Person(name, occupation, ageCategory, empCategory,
                taxId, isLtCitizen, genderCat);

        db.addPerson(person);
    }

    public void saveToFile(File file) throws IOException {
        db.saveToFile(file);
    }

    public void loadFromFile(File file) throws IOException {
        db.loadFromFile(file);
    }
}



