

import model.*;

import java.sql.SQLException;

public class TestDatabase {
    public static void main(String[] args) {
        String sql = "delete from employee where emp_id=1";
        System.out.println("runing databese test"); // mano testavimas

        Database db = new Database();
        try {
            db.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        db.addPerson(new Person("Joe", "Statgybininkas", AgeCategory.adult, EmploymentCategory.employed, "123456789", true, Gender.male));
        db.addPerson(new Person("Tomas", "sokejas", AgeCategory.child, EmploymentCategory.employed, "", false, Gender.male));



        try {
            db.save();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            db.load();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        db.disconnect();
    }
}
