import java.io.FileWriter;
import java.io.IOException;

class WriteToFile {
    public static void main(String[] args) {
        try {
            FileWriter myWriter = new FileWriter("src\\main\\resources\\PrefsDialog.properties");
            myWriter.write("name=root\n");
            myWriter.write("password=\n");
            myWriter.write("connection=3306");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}